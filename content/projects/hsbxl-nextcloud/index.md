---
title: "Our Own Cloud-out-of-the-cloud storage"
linktitle: "Nextcloud @HSBXL"
date: 2022-09-10
state: running
maintainer: "jurgen"
---

# Context and background
Let's set up a server running [Nextcloud](https://nextcloud.com/) and [Collabora Office](https://www.collaboraoffice.com/collabora-online-and-nextcloud/) inside the space. This will allow for:
- easily sharing documents
- creating project documentation that can then be shared publicly
- create spin-off applications that interact with a nextcloud ecosystem. Think of a ~~sports tracker~~ (who are we kidding?) *shopping list app* that stores its data in a nextcloud folder instead of a centralized service)
- see if we can do something with the [SOLID](https://solidproject.org/) concept that the Flemish government is working on (decentralized cryptographically safe storage of your personal information). There exists a [rudimentary POC for SOLID inside Nextcloud](https://apps.nextcloud.com/apps/solid). Can we set up our own pods and maybe even learn some things along the way?

Jurgen has set up Nextcloud on a local computer in the past - it's not that hard.

# Available material
- proxmox instance on the local server (still under configuration)

# To Do
- [ ] set up a proxmox device (8GB or RAM, 500 GB disk space should be plenty for starters)
- [ ] install and configure Nextcloud
- [ ] set up Nextcloud groups for members (more storage), guests (small storage), ideally aligned with LDAP
- [ ] configure [LDAP](https://docs.nextcloud.com/server/latest/admin_manual/configuration_user/user_auth_ldap.html) for user management
- [ ] Set up IPv6 to make Askarel happy (and for improved security)
- [ ] configuration of reverse proxy to make the cloud accessible from the outside and blazingly fast from the inside (LAN).

# Nice to have additionals - to be discussed
- [ ] create a dummy account ([Questular Rontok](https://www.imdb.com/title/tt0371724/characters/nm0151250)) that you are allowed to try hacking.
- [ ] organize a workshop on Nextcloud API
- [ ] organize a workshop on Nextcloud and SOLID
