---
startdate:  2019-01-31
starttime: "20:00"
enddate:  2019-01-31
allday: false
linktitle: "Using GNU Radio for Analog and Digital Communications"
title: "Using GNU Radio for Analog and Digital Communications"
location: "HSBXL"
eventtype: "Workshop"
price: "Free"
series: "byteweek2019"
image: "gnuradio.png"
alias: [events/byteweek/gnuradio-amateur-radio]
--- 

The way we send information, whether voice, text, images, or video, has 
been evolving since Samuel Morse’s telegraph in 1836. As these systems 
become more advanced the standard electronics tools and knowledge which 
have been the engineer's standard toolkit must be accompanied by 
software components. Together the analog, digital, and software are used 
to enable the modern communications modes such as Digital Video 
Broadcast (DVB), JT65 for Amateur Radio moonbounce, FreeDV which has 
brought digital voice to the HF bands, and the wide variety of other new 
and exciting protocols.

GNU Radio (www.gnuradio.org) is a free, graphical, software development 
toolkit that provides signal processing blocks to implement 
software-defined radios and signal-processing systems. It can be used 
with external RF hardware to create software-defined radios, or without 
hardware in a simulation environment. This workshop introduces the 
software, demonstrates assembling complete transmit and receive systems, 
and shows a few examples of advanced applications.
 
https://www.meetup.com/hackerspace-Brussels-hsbxl/events/258061306/

------------ cut here ----------- cut here ------------ cut here ------------

Hi all,

As the "Using GNU Radio for Analog and Digital Communications" workshop 
is ... well, as it name implies, a workshop, active involvement of the 
participants is expected.
To maximise the time available for the workshop, please note the 
following remarks found below;

1/ Bring your laptop:
As GNU Radio is a software toolkit, you need a computer to run this 
software.
It does not need to be the latest super-top-of-range machine. Any laptop 
not older then 4 to  5 years should be good enough to follow the workshop.

2/ Prepare a bootable USB flash-drive.
To allow the workshop to be focused on GNU Radio itself, and not "how do 
I install it", bootable USB-drives are used. These USB drives contain a 
complete ready-to-run system with all the software used during the 
workshop pre-installed on it.

A link to the ISO-image and the tools to create a suitable USB 
flash-drive can be found here:
https://wiki.gnuradio.org/index.php/GNU_Radio_Live_SDR_Environment

Please create a USB flash-drive with that image and try it on your 
laptop before-hand.

This would:
- allow us not to spend as little "workshop" time  as possible on this issue
- allow you to be sure that the software runs on your laptop.
- allow you to preconfigure the software to your particular laptop (e.g. 
keyboard-setting)

In any case, do not forget to bring your USB flash-drive to the workshop.

If  you would have any issues with this, you can purchased a USB 
flash-drive with a pre-installed ISO-image at the workshop. (at cost).
However, do drop us a note beforehand so we have an idea how many 
flash-drives we need to foresee.

Send a message to "kristoff (on1arf)" or "hsbxl" on the meetup attendees 
page if you need a prepared USB flash-drive:


3/ Also take a look at the preparation material found here:
- Introduction to GNU Radio and Software Radio:
https://wiki.gnuradio.org/index.php/Guided_Tutorial_Introduction

-Core concepts of GNU Radio:
https://wiki.gnuradio.org/index.php/TutorialsCoreConcepts

- Tutorial: GNU Radio Companion:
https://wiki.gnuradio.org/index.php/Guided_Tutorial_GRC

Enjoy the workshop