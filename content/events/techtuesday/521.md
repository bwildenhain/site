---
eventid: techtue521
startdate:  2019-05-07
starttime: "19:00"
linktitle: "TechTue 521"
title: "TechTuesday 521"
price: ""
image: "techtuesday.png"
series: "TechTuesday"
eventtype: "Social get-together"
location: "HSBXL"
---

Techtuesdays are a social meet-up sort of thing, just walk in for a talk, club-mate, latest news, or to show off your latest pet-project ... or you can always stay home and read slashdot.
