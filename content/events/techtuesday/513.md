---
eventid: techtue513
startdate:  2019-03-12
starttime: "19:00"
linktitle: "TechTue 513"
title: "TechTuesday 513"
price: ""
image: "techtuesday.png"
series: "TechTuesday"
eventtype: "Social get-together"
location: "HSBXL"
---

Techtuesdays are a social meet-up sort of thing, just walk in for a talk, club-mate, latest news, or to show off your latest pet-project ... or you can always stay home and read slashdot.


# De minuut
Everyone gets one minute to tell/ask/rant anything.  
https://www.youtube.com/watch?v=50W4NWsXwMs

# General assembly invitations need to be send!
https://hsbxl.be/events/ga/14

# Define agenda GA
Before the invitation is send, the agenda should be defined and made public.  
Because we have a monthly coremeeting where we can decide on things,  
the GA agenda should only be about accepting the bookkeeping and voting the board.

# Payment reminder Entrakt, again
betz called entract accounting today.
We need to contact Abel Falise to get our waranty back from the prevosu location  
WSI is working on this  
aska calls this week

# stat -L .hsbxl
Have a project from a member in the spotlight once in a while.  
Make an article on the website, and crosspost this on social media.  
To make things easier, let's make a question list we ask for each 'stat -L'

# Emails send by HSBXL?
Make an overview of all send out mails and put em in a repo on our Gitlab group.  
https://gitlab.com/hsbxl/hsbxl-mails