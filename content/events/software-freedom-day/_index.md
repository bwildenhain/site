---
linktitle: "Software Freedom Day"
title: "Software Freedom Day"
image: "sfd"

---

A get together with kids, teens and parents to work on tech projects and learn new stuff.

## Upcoming events
{{< events when="upcoming" series="SFD" >}}

## Past events
{{< events when="past" series="SFD" >}}
